# Training Backend

This repository contains the backend code developed on [Play Framework](https://www.playframework.com/) using Java and Scala.

Dependencies
---------------------
In order to install this project you first need to install [sbt](https://www.scala-sbt.org/)

Installation and Usage
----------------------

Clone or download the repository and run (on the project root)

       sbt compile

After that use 

       sbt run

The API will be up and running at [localhost:9000](http://localhost:9000/)

IDE
--------------------

IntelliJ, Eclipse, VS Code, Sublime