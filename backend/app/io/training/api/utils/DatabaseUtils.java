package io.training.api.utils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.util.JSON;
import com.typesafe.config.Config;
import io.training.api.mongo.serializers.SerializationAttributes;
import org.bson.Document;
import org.bson.conversions.Bson;
import play.Logger;
import play.libs.Json;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper class that formats data Created by Agon on 10/13/2016.
 */
public class DatabaseUtils {

	/**
	 * Convert the mongodb java driver find collection response to JSON
	 * ArrayNode which also mapps the _id -> id
	 *
	 * @param collection
	 * @return
	 */
	@Deprecated //todo rmv on pathsToJsonNode at PrestoJourneyPathsProcessor
	public static <T> ArrayNode toJsonNode(List<T> collection) {
		return DatabaseUtils.toJsonNode(collection, null);
	}

	/**
	 * Convert the mongodb java driver find collection response to JSON
	 * ArrayNode which also mapps the _id -> id
	 *
	 * @param collection
	 * @return
	 */
	public static <T> ArrayNode toJsonNode(List<T> collection, Config config) {
		ArrayNode result = Json.newArray();
		for (T transaction : collection) {
			JsonNode node = DatabaseUtils.toJsonNode(transaction, config);
			if (node != null)
				result.add(node);
		}
		return result;
	}

	/**
	 * Convert the mongodb java driver single object response to JsonNode
	 * @param <T>
	 *
	 * @param which
	 * @return
	 */
	@Deprecated //todo rmv on mapStats at MDPDataHandler and tests at Helper
	public static <T> JsonNode toJsonNode(T which) {
		return DatabaseUtils.toJsonNode(which, null);
	}

	/**
	 * Convert the mongodb java driver single object response to JsonNode
	 * @param <T>
	 * 
	 * @param which
	 * @return
	 */
	public static <T> JsonNode toJsonNode(T which, Config config) {
		ObjectMapper objectMapper = new ObjectMapper();

		// SET ENCRYPTION ATTRIBUTES
		if (config != null) {
			SerializationConfig serializationConfig = objectMapper.getSerializationConfig()
					.withAttribute(SerializationAttributes.PUBLIC_KEY_ATTRIBUTE, config.getString("encryption.public_key"))
					.withAttribute(SerializationAttributes.ENCRYPTION_TYPE_ATTRIBUTE, config.getString("encryption.type"));
			objectMapper.setConfig(serializationConfig);
		}

		objectMapper.configure(JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS, true);
		objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		if (which == null) {
			return null;
		}
		ObjectWriter writer = objectMapper.writerFor(which.getClass());
		ObjectReader reader = objectMapper.readerFor(JsonNode.class);
		if (which instanceof JsonNode) {
			return (JsonNode) which;
		}
		try {
			if (which instanceof Bson) {
				return reader.readValue(JSON.serialize(which));
			}
			return reader.readValue(writer.writeValueAsString(which));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
			Logger.of(DatabaseUtils.class).debug(which.toString());
		}
		return null;
	}

	/**
	 * Converts a json node into a class
	 * @param body
	 * @param valueType
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@Deprecated //todo missing
	public static <T> T jsonToJavaClass(JsonNode body, Class<T> valueType) throws JsonParseException, JsonMappingException, IOException {
		return DatabaseUtils.jsonToJavaClass(body, valueType, null);
	}

	/**
	 * Converts a json node into a class
	 * @param body
	 * @param valueType
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public static <T> T jsonToJavaClass(JsonNode body, Class<T> valueType, Config config) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper = objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

		// SET ENCRYPTION ATTRIBUTES
		if (config != null) {
			DeserializationConfig deserializationConfig = objectMapper.getDeserializationConfig()
				.withAttribute(SerializationAttributes.PRIVATE_KEY_ATTRIBUTE, config.getString("encryption.private_key"))
				.withAttribute(SerializationAttributes.ENCRYPTION_TYPE_ATTRIBUTE, config.getString("encryption.type"));
			objectMapper.setConfig(deserializationConfig);
		}

		ObjectReader reader = objectMapper.readerFor(valueType);
		return reader.readValue(body);
	}


	/**
	 * Parses an array node to a list of documents
	 * @param json
	 * @return
	 */
	public static List<Document> toListDocument(ArrayNode json) {
		List<Document> result = new ArrayList<>();
		for (JsonNode node : json) {
			result.add(toDocument((ObjectNode) node));
		}
		return result;
	}



	public static JsonNode fileToObjectNode (File which) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonParser parser = mapper.getFactory().createParser(which);
		JsonNode config = null;
		if (parser.nextToken() == JsonToken.START_OBJECT) {
			config = mapper.readTree(parser);
		}
		parser.close();
		return config;
	}

	/**
	 * parses a JSON object node and converts it to a mongodb java driver
	 * Document
	 * 
	 * @param value
	 * @return
	 */
	public static Document toDocument(ObjectNode value) {
		return Document.parse(value.toString());
	}
}
