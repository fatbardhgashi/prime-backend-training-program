package io.training.api.services;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.typesafe.config.Config;
import io.training.api.exceptions.RequestException;
import io.training.api.utils.DatabaseUtils;
import org.bson.Document;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Http;
import play.mvc.Http.Request;

import javax.inject.Singleton;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.Executor;

/**
 * Created by Agon on 09/08/2020
 */
@Singleton
public class SerializationService {
    @Inject
    HttpExecutionContext ec;
    @Inject
    Config config;

    public <T> CompletableFuture<JsonNode> toJsonNode(List<T> result) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                return DatabaseUtils.toJsonNode(result, config);
            } catch (Exception ex) {
                throw new CompletionException(new RequestException(Http.Status.BAD_REQUEST, "parsing_exception"));
            }
        }, ec.current());
    }

    public <T> CompletableFuture<JsonNode> toJsonNode(CompletableFuture<T> future) {
        return future.thenCompose((result) -> {
            if (result instanceof List<?>) {
                return this.toJsonNode((List<?>) result);
            }
            return this.toJsonNode(result);
        });
    }

    public <T> CompletableFuture<JsonNode> toJsonNode(T result) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                JsonNode node = null;
                if (result instanceof List<?>) {
                    node = DatabaseUtils.toJsonNode((List<?>) result, config);
                } else {
                    node = DatabaseUtils.toJsonNode(result, config);
                }
                if (node == null) {
                    throw new RequestException(Http.Status.BAD_REQUEST, "parsing_exception");
                }
                return node;
            } catch (RequestException ex) {
                throw new CompletionException(ex);
            } catch (Exception ex) {
                throw new CompletionException(new RequestException(Http.Status.BAD_REQUEST, "parsing_exception"));
            }
        }, ec.current());
    }

    public CompletableFuture<Document> parseBody(Request request) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                JsonNode json = request.body().asJson();
                if (!json.isObject()) {
                    throw new RequestException(Http.Status.BAD_REQUEST, "invalid_parameters");
                }
                return DatabaseUtils.toDocument((ObjectNode) json);
            } catch (RequestException ex) {
                throw new CompletionException(ex);
            } catch (Exception ex) {
                throw new CompletionException(new RequestException(Http.Status.BAD_REQUEST, "parsing_exception"));
            }
        }, ec.current());
    }

    public <T> CompletableFuture<T> parseBodyOfType(Request request, Class<T> valueType) {
        return CompletableFuture.supplyAsync(() -> {
            try {
				JsonNode body = request.body().asJson();
                return DatabaseUtils.jsonToJavaClass(body, valueType, config);
            } catch (JsonParseException|JsonMappingException e) {
                e.printStackTrace();
                throw new CompletionException(new RequestException(Http.Status.BAD_REQUEST, "parsing_exception"));
            } catch (IOException e) {
                e.printStackTrace();
                throw new CompletionException(new RequestException(Http.Status.BAD_REQUEST, "parsing_exception"));
            }  catch (Exception e) {
                e.printStackTrace();
                throw new CompletionException(new RequestException(Http.Status.INTERNAL_SERVER_ERROR, "service_unavailable"));
            }
        }, ec.current());
    }

    public CompletableFuture<List<Document>> parseListBody (Request request, Executor context) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                JsonNode json = request.body().asJson();
                if(!json.isArray()) {
                    throw new RequestException(Http.Status.BAD_REQUEST, "invalid_parameters");
                }
                return DatabaseUtils.toListDocument((ArrayNode) json);
            } catch (RequestException ex) {
                throw new CompletionException(ex);
            } catch (Exception e) {
                e.printStackTrace();
                throw new CompletionException(new RequestException(Http.Status.INTERNAL_SERVER_ERROR, "service_unavailable"));
            }
        }, ec.current());
    }

    public <T> CompletableFuture<List<T>> parseListBodyOfType(Request request, Class<T> type) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                List<T> items = new ArrayList<>();
                JsonNode json = request.body().asJson();
                if(!json.isArray()) {
                    throw new RequestException(Http.Status.BAD_REQUEST, "invalid_parameters");
                }
                for(JsonNode node : (ArrayNode) json ) {
                    items.add(DatabaseUtils.jsonToJavaClass((JsonNode) node, type, config));
                }
                return items;
            } catch (JsonParseException|JsonMappingException e) {
                e.printStackTrace();
                throw new CompletionException(new RequestException(Http.Status.BAD_REQUEST, "parsing_exception"));
            } catch (IOException e) {
                e.printStackTrace();
                throw new CompletionException(new RequestException(Http.Status.BAD_REQUEST, "parsing_exception"));
            }  catch (Exception e) {
                e.printStackTrace();
                throw new CompletionException(new RequestException(Http.Status.INTERNAL_SERVER_ERROR, "service_unavailable"));
            }
        }, ec.current());
    }

    public <T> CompletableFuture<T> parseFileOfType(Request request, String key, Class<T> valueType) {
        return CompletableFuture.supplyAsync(() -> {
            Http.MultipartFormData<File> data = request.body().asMultipartFormData();
            if (data.getFiles().size() == 0) {
                throw new IllegalArgumentException("invalid_parameters");
            }
            File file = data.getFile(key).getFile();
            try {
                JsonNode content = DatabaseUtils.fileToObjectNode(file);
                return DatabaseUtils.jsonToJavaClass(content, valueType, config);
            } catch (IOException e) {
                throw new CompletionException(new RequestException(Http.Status.BAD_REQUEST, "parsing_exception"));
            }
        }, ec.current());
    }

    public static <T> Document javaClassToDocument(T valueType) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

		try {
			return Document.parse(objectMapper.writeValueAsString(valueType));
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
