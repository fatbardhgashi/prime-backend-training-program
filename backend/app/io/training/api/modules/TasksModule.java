package io.training.api.modules;

import com.google.inject.AbstractModule;
import play.libs.akka.AkkaGuiceSupport;

public class TasksModule extends AbstractModule implements AkkaGuiceSupport {

    @Override
    protected void configure() {
    }
}